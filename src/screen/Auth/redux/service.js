import {URL_SIGN_IN, URL_SIGN_UP} from "@src/constant/url"
import {POST, GET} from "@src/redux/server"

const Authservice = {
  signIn: async ({email, password}) => {
    const url = URL_SIGN_IN
    const params = {
      scope: "ui",
      grant_type: "password",
      username: email,
      password: password
    }
    return await GET({url: url, basic: true, params})
  },
  signUp: async ({email, password}) => {
    const url = URL_SIGN_UP
    const body = {email: email, password: password}
    return await POST({url, body})
  }
}

export default Authservice
